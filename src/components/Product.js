import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom'
import {useDispatch, useSelector} from "react-redux";
import {getProduct} from "../redux/actions/productAction";
import "react-alice-carousel/lib/alice-carousel.css";
import Carousel from "./Carousel";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'

const Product = () => {

    const {id} = useParams();
    const dispatch = useDispatch();

    const {product, loading} = useSelector(state => state.product);

    useEffect(() => {

        dispatch(getProduct(id));

    }, [id]);


    return (
        <div className="container my-5">


            <div className="row">
                <div className="col-md-6">

                    {
                        !loading ? <Carousel imageData={product?.images}/> :

                            <SkeletonTheme baseColor="#F7F8F8" highlightColor="#e8e9e9">
                                <Skeleton count={1} height="400px" />
                            </SkeletonTheme>

                    }

                </div>
                <div className="col-md-6">

                    {
                        !loading ? <Card sx={{maxWidth: 500, height: "100%"}}>
                                <CardMedia
                                    component="img"
                                    alt="green iguana"
                                    height="350"
                                    image={product?.thumbnail}
                                />
                                <CardContent>
                                    <Typography className="products-title" gutterBottom variant="h5" component="div">
                                        {product?.title}
                                    </Typography>
                                    <Typography className="mb-4" variant="body1" color="text.secondary">
                                        {product?.description}
                                    </Typography>

                                    <Typography className="mb-2" variant="body1" color="text.secondary">
                                        <b>Category: </b>{product?.category}
                                    </Typography>

                                    <Typography className="mb-2" variant="body1" color="text.secondary">
                                        <b>Brand: </b>{product?.brand}
                                    </Typography>

                                    <Typography className="mb-2" variant="body1" color="text.secondary">
                                        <b>Price: </b>{product?.price}
                                    </Typography>

                                    <Typography className="" variant="body1" color="text.secondary">
                                        <b>Rating: </b>{product?.rating}
                                    </Typography>


                                </CardContent>


                            </Card> :

                            <SkeletonTheme baseColor="#F7F8F8" highlightColor="#e8e9e9">

                                <Card sx={{ maxWidth: 500, height: "100%" }}>

                                    <Skeleton count={1} height="350px" />

                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            <Skeleton count={1} />
                                        </Typography>
                                        <Typography variant="body1" color="text.secondary">
                                            <Skeleton count={3} />
                                        </Typography>


                                        <Typography className="mb-2 mt-2 d-flex" variant="body1" color="text.secondary">

                                            <Skeleton count={4} width="80px"/>

                                            <Skeleton className="ms-2" count={4} width="80px"/>

                                        </Typography>


                                    </CardContent>
                                </Card>

                            </SkeletonTheme>
                    }


                </div>
            </div>

        </div>
    );
};

export default Product;