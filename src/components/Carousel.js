import React from "react";
import Slider from "react-slick";
import "../style/carousel.scss"
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

export default function Carousel({imageData}) {

    const settings = {
        infinite: true,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 1600,
        pauseOnhover: true,
        centerMode: true,
        centerPadding: '100px',
        slidesToShow: 1,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    };

    console.log(imageData);

    return (
        <Slider {...settings}>

            {
                imageData?.map((image)=>(
                    <div className="carousel-item">

                        <Card sx={{ maxWidth: 345, height: "100%" }}>
                            <CardMedia
                                component="img"
                                alt="green iguana"
                                height="250"
                                image={image}
                            />
                        </Card>
                    </div>
                ))
            }

        </Slider>
    );
}