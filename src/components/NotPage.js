import React from 'react';

const NotPage = () => {
    return (
        <div>
            Page Not Found
        </div>
    );
};

export default NotPage;