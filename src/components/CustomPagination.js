import React, {useEffect} from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import {useSelector} from "react-redux";


const CustomPagination = ({page, setPage }) => {

    const {numberOfPages} = useSelector((state) => state.product);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    return (
        <Stack spacing={1}>
            <Pagination
                page={page}
                variant="outlined"
                shape="rounded"
                count={numberOfPages}
                onChange={handleChangePage}
            />
        </Stack>
    );
};

export default CustomPagination;