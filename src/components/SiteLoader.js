import React from 'react';
import { SemipolarLoading } from 'react-loadingg';

function SiteLoader(props) {
    return (
        <div className="SiteLoader">
            <SemipolarLoading size={"large"} color={"#6C757D"}/>
        </div>
    );
}

export default SiteLoader;