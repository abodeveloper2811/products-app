import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {getCategory, getProducts} from "../redux/actions/productAction";
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton'
import {Link} from "react-router-dom";
import CustomPagination from "./CustomPagination";
import SearchIcon from '@mui/icons-material/Search';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const Products = () => {

    const dispatch = useDispatch();

    const [request, setRequest] = useState(false);

    const {products, loading, numberOfPages, category} = useSelector((state) => state.product);

    const [page, setPage] = useState(1);
    const [searchText, setSearchText] = useState('');
    const [changedCategory, setChangedCategory] = useState('All');

    useEffect(()=>{
       dispatch(getCategory());
    },[]);

    useEffect(() => {
        dispatch(getProducts(searchText, changedCategory, page));

        setTimeout(()=>{
           setRequest(true);
        }, 900 );

    }, [page]);

    const handleChangeName = (event) =>{
        setSearchText(event.target.value);
    };

    const handleChangeCategory = (event) =>{
        setChangedCategory(event.target.value);
        setPage(1);
        dispatch(getProducts(searchText, event.target.value, 1))
    };

    const filterNameData = () =>{
        setPage(1);
        dispatch(getProducts(searchText, changedCategory, 1))
    };

    return (
        <div className="container my-4">

            <div className="row">
                <div className="col-md-12">

                    <div className="top d-flex align-items-center justify-content-between">
                            <div className="search d-flex">

                                    <TextField
                                        label="Input for search"
                                        id="outlined-size-small"
                                        value={searchText}
                                        size="small"
                                        onChange={handleChangeName}
                                    />

                                    <button onClick={()=>filterNameData()} type="button" className="btn btn-secondary">
                                        <SearchIcon/>
                                    </button>

                            </div>

                            <div className="category">
                                <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                                    <InputLabel id="demo-select-small">Category</InputLabel>
                                    <Select
                                        labelId="demo-select-small"
                                        id="demo-select-small"
                                        value={changedCategory}
                                        label="Category"
                                        onChange={handleChangeCategory}
                                    >
                                        <MenuItem value="All">
                                            All
                                        </MenuItem>

                                        {
                                            category.map((category)=>(
                                                <MenuItem value={category}>{category}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </div>
                    </div>

                </div>
                <div className="col-md-12">

                    <h1 className="text-center mb-3 title">Our products</h1>

                    <div className="row">
                        {

                            !loading ?

                                products?.length !==0 ? products.map((product) => (
                                <div className="col-md-4 my-2">

                                    <Card sx={{ maxWidth: 345, height: "100%" }}>
                                    <CardMedia
                                        component="img"
                                        alt="green iguana"
                                        height="250"
                                        image={product.thumbnail}
                                    />
                                    <CardContent>
                                        <Typography className="products-title" gutterBottom variant="h5" component="div">
                                            {product.title}
                                        </Typography>
                                        <Typography height="40px" variant="body2" color="text.secondary">
                                            {product.description}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>

                                        <Link to={`products/${product.id}`} style={{textDecoration: "none"}}>
                                            <Button size="small">Learn More</Button>
                                        </Link>

                                    </CardActions>
                                </Card>

                                </div>
                            )) :
                                    request ?

                                    <h3 className="not-found">Nothing found !!!</h3> : ""

                                :

                                <div className="col-md-12">

                                    <div className="row">
                                        <div className="col-md-4">
                                            <SkeletonTheme baseColor="#F7F8F8" highlightColor="#e8e9e9">

                                                <Card sx={{ maxWidth: 345, height: "100%" }}>

                                                    <Skeleton count={1} height="250px" />

                                                    <CardContent>
                                                        <Typography gutterBottom variant="h5" component="div">
                                                            <Skeleton count={1} />
                                                        </Typography>
                                                        <Typography variant="body2" color="text.secondary">
                                                            <Skeleton count={3} />
                                                        </Typography>
                                                    </CardContent>
                                                    <CardActions>
                                                            <Skeleton className="ms-2"  count={1} width="90px" height="30px"/>
                                                    </CardActions>
                                                </Card>

                                            </SkeletonTheme>
                                        </div>

                                        <div className="col-md-4">
                                            <SkeletonTheme baseColor="#F7F8F8" highlightColor="#e8e9e9">

                                                <Card sx={{ maxWidth: 345, height: "100%" }}>

                                                    <Skeleton count={1} height="250px" />

                                                    <CardContent>
                                                        <Typography gutterBottom variant="h5" component="div">
                                                            <Skeleton count={1} />
                                                        </Typography>
                                                        <Typography variant="body2" color="text.secondary">
                                                            <Skeleton count={3} />
                                                        </Typography>
                                                    </CardContent>
                                                    <CardActions>
                                                        <Skeleton className="ms-2"  count={1} width="90px" height="30px"/>
                                                    </CardActions>
                                                </Card>

                                            </SkeletonTheme>
                                        </div>

                                        <div className="col-md-4">
                                            <SkeletonTheme baseColor="#F7F8F8" highlightColor="#e8e9e9">

                                                <Card sx={{ maxWidth: 345, height: "100%" }}>

                                                    <Skeleton count={1} height="250px" />

                                                    <CardContent>
                                                        <Typography gutterBottom variant="h5" component="div">
                                                            <Skeleton count={1} />
                                                        </Typography>
                                                        <Typography variant="body2" color="text.secondary">
                                                            <Skeleton count={3} />
                                                        </Typography>
                                                    </CardContent>
                                                    <CardActions>
                                                        <Skeleton className="ms-2"  count={1} width="90px" height="30px"/>
                                                    </CardActions>
                                                </Card>

                                            </SkeletonTheme>
                                        </div>

                                    </div>

                                </div>

                        }

                    </div>

                    <div className="row mt-4">
                        <div className="col-md-12 text-center d-flex align-items-center justify-content-center">

                            {
                                numberOfPages > 1 ?
                                    <CustomPagination page={page} setPage={setPage}/>
                                    : ""
                            }

                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default Products;