import axios from 'axios';
import {PATH_NAME} from "../../tools/constant";
import {
    GET_CATEGORY,
    GET_FILTER_PRODUCTS,
    GET_PRODUCT,
    GET_PRODUCTS,
    LOADING_FALSE,
    LOADING_TRUE
} from "../actionTypes/productActionsTypes";

export const getProduct = (productId) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}/product/${productId}`);

        setTimeout(()=>{
            dispatch({
                type: GET_PRODUCT,
                payload: res.data
            });
        }, 800);


    } catch (err) {
        dispatch({
            LOADING_FALSE
        })
    }
};

export const getProducts = (name, category, page) => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    let requestCategory;

    if (category === 'All'){
        requestCategory = '';
    } else{
        requestCategory = category;
    }

    try {

        const res = await axios.get(`${PATH_NAME}/product?name=${name}&category=${requestCategory}&limit=3&offset=${page*3-3}`);

        setTimeout(()=>{
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data
            });
        }, 800);

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });
    }

};

export const getCategory = () => async (dispatch) => {

    dispatch({
        type: LOADING_TRUE
    });

    try {

        const res = await axios.get(`${PATH_NAME}/category`);

        dispatch({
            type: GET_CATEGORY,
            payload: res.data
        });

    } catch (err) {
        dispatch({
            type: LOADING_FALSE,
        });
    }

};











