import {GET_CATEGORY, GET_PRODUCT, GET_PRODUCTS, LOADING_FALSE, LOADING_TRUE} from "../actionTypes/productActionsTypes";

const initialState = {
    products: [],
    product: {},
    category: [],
    filterProducts: [],
    numberOfPages: null,
    loading: false,
};

export const productReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case GET_PRODUCT:
            return {
                ...state,
                product: payload,
                loading: false,
            };

        case GET_PRODUCTS:

            let pages;

            if (payload.count % 3 === 0) {
                pages = parseInt(payload.count / 3);
            } else {
                pages = parseInt(payload.count / 3) + 1;
            }

            return {
                ...state,
                products: payload.products,
                numberOfPages: pages,
                loading: false,
            };

        case GET_CATEGORY:
            return {
                ...state,
                category: payload,
                error: null,
                loading: false,
            };

        case LOADING_TRUE:
            return {
                ...state,
                loading: true,
            };

        case LOADING_FALSE:
            return {
                ...state,
                loading: false,
            };

        default:
            return state;
    }
};


