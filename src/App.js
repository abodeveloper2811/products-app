import './App.css';
import {useEffect, useState} from "react";
import {Route, Routes} from "react-router-dom";
import Products from "./components/Products";
import NotPage from "./components/NotPage";
import Product from "./components/Product";
import SiteLoader from "./components/SiteLoader";

function App() {

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            setLoading(false);
        }, 1000)
    }, []);

    if (loading) {
        return (
            <SiteLoader/>
        )
    }

    return (
        <div className="App">

            <Routes>
                <Route path="/">
                    <Route index element={<Products/>}/>
                    <Route path="products/:id" element={<Product/>}/>
                    <Route path="*" element={<NotPage/>}/>
                </Route>
            </Routes>

        </div>
    );
}

export default App;
